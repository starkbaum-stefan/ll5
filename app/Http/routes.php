<?php

#Articles

//Route::get('articles', 'ArticlesController@index');
//Route::get('articles/create', 'ArticlesController@create');
//Route::get('articles/{id}', 'ArticlesController@show');
//Route::post('articles', 'ArticlesController@store');

Route::get('/', function() {
    return redirect('articles');
});

Route::resource('articles', 'ArticlesController');

Route::get('contact', 'PagesController@contact');
Route::get('about', 'PagesController@about');