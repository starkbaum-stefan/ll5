<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PagesController extends Controller {

    public function contact() {
        $email = 'starkbaum.stefan@gmail.com';
        return view('pages.contact')->with('email', $email);
    }

	public function about() {
        $first = 'Stefan';
        $last = 'Starkbaum';
        $people = [
            'Taylor Otwell', 'Dayle Rees', 'Eric Barnes'
        ];
        return view('pages.about',compact('first', 'last', 'people'));
    }

}
